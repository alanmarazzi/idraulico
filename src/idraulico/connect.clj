(ns idraulico.connect
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.java.io :as io]
            [clojure.edn :as edn])
  (:import [java.io PushbackReader]))


(defn edn-reader
  [path-to-edn]
  (edn/read
    (PushbackReader.
      (io/reader path-to-edn))))


(def config
  (atom (edn-reader "connections")))


(defn configurator
  "Read and store connections"
  ([] (atom (edn-reader "config")))
  ([path] (atom (edn-reader path))))


(defmulti connection (fn [db] (:db db)))


(defmethod connection "mysql"
  [{:keys [db host port dbname usr pswd]}]
  {:dbtype   db
   :dbname   dbname
   :host     host
   :port     port
   :user     usr
   :password pswd})


(jdbc/query
  (connection (:connection @(configurator)))
  "select * from employees limit 5")




