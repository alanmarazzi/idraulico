(ns velocissimo.config
  (:require [clojure.java.io :as io]
            [clojure.edn :as edn]))


(defn read-job
  [job-path]
  (:job
   (edn/read
     (java.io.PushbackReader.
       (io/reader job-path)))))


(defn get-job-step
  [job name]
  (->> job
       (filter #(= (:dbname (:db %)) name))
       first))


(def cf-schema
  (atom {:anno     [6 8]
         :mese     [8 9]
         :mese-cod {"A" 1
                    "B" 2
                    "C" 3
                    "D" 4
                    "E" 5
                    "H" 6
                    "L" 7
                    "M" 8
                    "P" 9
                    "R" 10
                    "S" 11
                    "T" 12}
         :giorno   [9 11]
         :sesso    40
         :flag-str [11 12]}))
