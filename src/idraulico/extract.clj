(ns velocissimo.extract
  (:require [clojure.java.io :as io]
            [clojure.string :as string]
            [clojure.data.csv :as csv]
            [clojure.java.jdbc :as jdbc]
            [honeysql.core :as sql]
            [honeysql.helpers :as sqlhelp]
            [clj-time.jdbc]
            [clj-time.core :as datetime]))


(defn subfield
  "Split a string at given `start` `end`
  values:
    `(subfield 'test me'
               [2 4])
     => 'st m'`"
  [line [start end]]
  (string/trim (subs line start end)))


(defn read-csv
  [filename]
  (with-open [rdr  (io/reader (str filename ".csv"))
              head (io/reader
                     (str "header" filename ".csv"))]
    (let [values (csv/read-csv rdr)
          header (map keyword
                      (first (csv/read-csv head)))]
      (doall
        (map #(zipmap header %) values)))))


(defn write-csv
  "Write the given vector as a csv file"
  [out-name data]
  (with-open [writer (io/writer out-name)]
    (csv/write-csv writer data)))


(defn write-csv-with-header
  [out-name data]
  (let [k (map name (keys (first data)))
        v (map vals data)
        d (cons k v)]
    (write-csv out-name d)))


(defn write-temp-table!
  "Write the given data as a csv with a separated
  csv file for its header"
  [{:keys [out-file]} data]
  (do
    (write-csv out-file (map vals data))
    (write-csv (str "header" out-file) 
               (vector (map name (keys (first data)))))))


(defn config-map->map-steps
  [step k]
  (->> (k step)
       seq
       (map #(apply hash-map %))
       (map #(assoc % :function k))))


(defn keep-or-discard
  [steps]
  (let [k  (config-map->map-steps steps :keep)
        d  (config-map->map-steps steps :discard)
        in (config-map->map-steps steps :in)]
    (concat k d in)))


(defn parse-steps
  [steps]
  (let [s (keep-or-discard steps)]
    (for [step s]
      (let [f (:function step)
            d (dissoc step :function)
            k (first (keys d))
            v (first (vals d))]
        (cond
          (= :keep f)    [:=  k v]
          (= :discard f) [:<> k v]
          (= :in f)      [:in k v])))))


(defn make-where
  [job-map {:keys [explain]}]
  (let [steps (keep :transform explain)]
    (if (seq steps)
      (apply sqlhelp/merge-where job-map (mapcat parse-steps steps))
      job-map)))


(defn parse-join
  [master explain]
  (loop [m    (sequence [])
         step (first explain)
         r    (rest explain)]
    (if step
      (let [table (:table step)
            join  (:left-join step)
            ks    (first join)]
        (if join
          (recur (conj m [(keyword table) 
                          [:= (keyword (str master "." (first ks)))
                           (keyword (str table "." (second ks)))]])
               (first r)
               (rest r))
          (recur m
                 (first r)
                 (rest r))))
        (apply concat m))))


(defn make-join
  [job-map {:keys [master explain]}]
  (apply sqlhelp/left-join job-map (parse-join master explain)))


(defn parse-fields
  [{:keys [table fields]}]
  (map #(keyword (str table "." %)) fields))


(defmulti make-select 
  (fn [job-map] 
    (let [dbtype (:dbtype (:db job-map))]
      (if (= dbtype "postgresql")
        dbtype
        :sqlassic))))


(defmethod make-select :sqlassic
  [{:keys [explain]}]
  (let [fields (mapcat parse-fields explain)]
    (if (seq fields)
      (apply sqlhelp/select fields)
      (sqlhelp/select :*))))


(defmethod make-select "postgresql"
  [{:keys [explain]}]
  (let [apexize (fn [step]
                  {:table (:table step)
                   :fields (map #(str "\"" % "\"")
                                   (:fields step))})
        fields (map apexize explain)]
    (if (seq fields)
      (apply sqlhelp/select (mapcat parse-fields fields))
      (sqlhelp/select :*))))


(defn make-from
  [job-map {:keys [explain]}]
  (sqlhelp/from job-map (:table (first explain))))


(defn make-query-map
  [job]
  (as-> (make-select job) q
        (make-from q job)
        (make-join q job)
        (make-where q job)))


(defn make-query
  [query-map]
  (sql/format query-map))


(defn send-query
  [db query]
  (do
    (println "Sending query to " (:dbname db))
    (let [res (jdbc/query db query)]
      (println "Finished retrieving " (:dbname db))
      res)))
