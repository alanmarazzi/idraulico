(ns velocissimo.transform
  (:require [clojure.java.io :as io]
            [clojure.data.csv :as csv]
            [velocissimo.config :as config
             :reload true]
            [velocissimo.extract :as extract
             :reload true]
            [clojure.set :as set]
            [simple-time.core :as simpletime]
            [clojure.string :as string]))


(defn left-join
  "Helper function to do a left-join"
  [left-keys right-keys]
  left-keys)


(defn- map-combine
  [group1 group2 dummy-map]
  (for [map1 group1
        map2 group2]
    (merge-with #(or %2 %1) dummy-map map2 map1)))


(defn joiner
  "Returns the join of two collections of maps"
  [left-coll right-coll left-fn right-fn join-type]
  (let [left-idx  (group-by left-fn left-coll)
        right-idx (group-by right-fn right-coll)
        join-keys (set (join-type (keys left-idx)
                                  (keys right-idx)))]
    (mapcat #(map-combine (get left-idx  % [{}])
                          (get right-idx % [{}])
                          (zipmap (set (set/union
                                         (keys (first left-coll))
                                         (keys (first right-coll))))
                                  (repeat nil)))
            join-keys)))


(defn- parse-int
  [number-string]
  (Integer/parseInt number-string))


(defn- get-incidentid
  [row]
  (second (string/split (:incidentid row) #":")))


(defn- current-year
  []
  (simpletime/datetime->year (simpletime/today)))


(defn- cf-year
  [cf schema]
  (parse-int (extract/subfield cf (:anno schema))))


(defn- cf-day
  [cf schema]
  (parse-int (extract/subfield cf (:giorno schema))))


(defn- cf-sex
  [cf day schema]
  (if (> day (:sesso schema)) "F" "M"))


(defn- cf-real-day
  [cf schema]
  (let [day (cf-day cf schema)
        sex (cf-sex cf day schema)]
    (if (= sex "F")
      (- day (:sesso schema))
      day)))


(defn- cf-age
  [cf-year]
  (if (> (- (current-year) (+ 1900 cf-year)) 99) 
    (- (current-year) (+ 2000 cf-year))
    (- (current-year) (+ 1900 cf-year))))


(defn- cf-foreign
  [cf schema]
  (if (=
        (extract/subfield cf (:flag-str schema))
        "Z")
    1
    0))


(defn birth
  "Given a fiscal code returns sex, age and nationality"
  [row]
  (try
    (let [schema  @config/cf-schema
          cf      (:sptcfisc row)
          sex     (cf-sex cf (cf-day cf schema) schema)
          age     (cf-age (cf-year cf schema))
          foreign (cf-foreign cf schema)]
      {:age       age
       :SESSO_ana sex
       :flag_str  foreign})
    (catch StringIndexOutOfBoundsException e (println (str e))
           {:age      nil
            :sex      nil
            :flag_str nil})))


(defn- wrap-add
  ([row f]
   (conj row (f row)))
  ([row f k]
   (let [r (f row)]
     (conj row [k r]))))


(defn- crash-age
  [row]
  (let [happening  
        (simpletime/datetime->year 
          (simpletime/parse 
            (str (:ssidtden row))))]
    (- (:age row) 
       (- (simpletime/datetime->year 
            (simpletime/today))
          happening))))


(defn- missing?
  [value]
  (if (or (nil? value)
          (string/blank? value))
    true
    false))


(defn- def-missing
  [original missing-val]
  (if (missing? original)
    missing-val
    original))


(defn- vehicle-age
  [row]
  (when-not (nil? (:sptdtimm row))
    (- (simpletime/datetime->year
         (simpletime/datetime))
       (simpletime/datetime->year
         (simpletime/parse (str (:sptdtimm row)))))))


(defn- claim-delay
  [row]
  (let [crash (simpletime/parse (str (:ssidtsin row)))
        claim (simpletime/parse (str (:ssidtden row)))]
    (simpletime/timespan->days (simpletime/- claim crash))))


(defn- has-convention
  [row]
  (if (zero? (:sptcconv row))
    0
    1))


(defn- has-9050
  [row]
  (if (= (:sptprod row) 9050)
    1
    0))


(defn- crash-day&month
  [row]
  (let [date  (simpletime/parse (str (:ssidtsin row)))
        day   (simpletime/datetime->day-of-week date)
        month (simpletime/datetime->month date)]
    {:mese   month
     :giorno day}))


(defn- check-fuel
  [value]
  (cond
    (= value "M")         "B"
    (string/blank? value) "B"
    :else                 value))


(defn- check-fractioning
  [value]
  (if (some #{"Semestrale" "Annuale"} 
            #{value})
    value
    "Altro"))


(defn- check-util
  [value]
  (if (some (set (map str (range 4)))
            #{value})
    value
    4))


(defn- check-vehicle
  [value]
  (if (some #{1 3}
            #{value})
    (str value)
    "0"))


(defn- check-use
  [value]
  (def-missing value "MM"))


(defn- align-names
  [row name-map]
  (set/rename-keys
    row
    name-map))


(defn- prov-missing
  [row]
  (let [prov-acc (def-missing (:sadtprov row) "MANCANTE")
        prov-ana (def-missing (:sptprov row) "MANCANTE")]
    (assoc row :sadtprov prov-acc :sptprov prov-ana)))


(defn- birth=crash?
  [row]
  (= (:sadtprov row)
     (:sptprov row)))


(defn- birth=crash
  [row]
  (if (birth=crash? row)
    (if (= (:sadtprov row)
           "MANCANTE")
      "M"
      "S")
    "N"))


(defn- long-weekend
  [row]
  (if (> (:giorno row)
         4)
    1
    0))


(defn zp
  "An helper to add zero padding to strings"
  [n &[c]]
  (if c
    (as-> c val
          (str "%0" val "d") 
          (format val n))
    (str n)))


(defn keys-to-upper
  "Changes keys to upper case"
  [m]
  (let [k (keys m)
        v (vals m)]
    (zipmap
      (map #(keyword
              (string/upper-case
                (name %)))
           k)
      v)))


(def xtrans
  (comp
    (map #(wrap-add % birth))
    (map #(wrap-add % vehicle-age :eta_veicolo))
    (map #(wrap-add % claim-delay :ritardo_denuncia))
    (map #(wrap-add % has-convention :flag_convenzione))
    (map #(wrap-add % has-9050 :flag_9050))
    (map #(update % :sptalim check-fuel))
    (map #(update % :sptfraz check-fractioning))
    (map #(update % :sptutilz check-util))
    (map #(update % :sptclvei check-vehicle))
    (map #(update % :sptuso check-use))
    (map #(wrap-add % crash-day&month))
    (map prov-missing)
    (map #(wrap-add % birth=crash :prov_ana_equal_acc))
    (map #(wrap-add % long-weekend :long_weekend))
    (map #(align-names % {:regione_out :regione
                          :flag_str    :FLAGSTR_ana
                          :age         :ETA_ANA}))
    (map keys-to-upper)))
