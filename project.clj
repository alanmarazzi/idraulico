(defproject idraulico "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [honeysql "0.9.2"]
                 [org.clojure/java.jdbc "0.7.6"]
                 [mysql/mysql-connector-java "8.0.11"]
                 [org.clojure/data.csv "0.1.4"]])
